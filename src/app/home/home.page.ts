import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public stories: any[] = [];

  constructor(private platform: Platform) {

  this.stories = [
    {title: 'Are Your Shorts Out to Get You?', author: 'Sutanito Rodriguez'},
    {title: 'IE8 Default Browser for New iPhone 9', author: 'John Doe'},
    {title: 'Door Handles Mysteriously Vanishing Leaves Thousands Trapped in Rooms', author: 'Fulanito Santaella'},
    {title: 'The Secret Health Benefits of Muffins', author: 'Perensejito Villanueva'},
    {title: 'Looting Begins as IE8 Riots Continue', author: 'Agapito Perdomo'},
    {title: 'Mysterious Object Eclipses Sun', author: 'Pedro Perez'},
    {title: 'Is Your Spouse Having an Affair with an AI?', author: 'Ramon Ramirez'},
    {title: 'Tourism Plummets as Virtual Holidays Gain Popularity', author: 'Joshua Smith'},
    {title: 'Does This Adelaide Man Have the Secret Formula for Winning the Lottery?', author: 'William Anderson'}
  ];

  }

}
